package fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController

import com.asonbekhar.mvvm2.R
import com.asonbekhar.mvvm2.databinding.FragmentScoreBinding


class ScoreFragment : Fragment() {
    lateinit var binding: FragmentScoreBinding
    var score = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding=FragmentScoreBinding.inflate(inflater)
       // FragmentScoreBinding.inflate(inflater).let{
         //   binding=it
       //
       //     return it.root
        //}
        //--------------------------
        //ScoreFragmentArgs.fromBundle(requireArguments()).score
        arguments?.let {
            score =ScoreFragmentArgs.fromBundle(it).score
            binding.tvShowScore.text=score.toString()
        }
        binding.button.setOnClickListener {
            findNavController().navigateUp()
        }

        return binding.root
    }








}
