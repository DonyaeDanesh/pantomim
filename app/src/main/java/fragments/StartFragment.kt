package fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController

import com.asonbekhar.mvvm2.R
import com.asonbekhar.mvvm2.databinding.FragmentStartBinding

class StartFragment : Fragment() {
lateinit var binding: FragmentStartBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding=FragmentStartBinding.inflate(inflater)
        binding.btnStart.setOnClickListener{
            findNavController().navigate(StartFragmentDirections.actionStartFragmentToGameFragment())
        }




        return binding.root
    }


}
