package fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.asonbekhar.mvvm2.GameViewModel

import com.asonbekhar.mvvm2.databinding.FragmentGameBinding
import kotlinx.android.synthetic.main.fragment_game.*


class GameFragment : Fragment() {
    lateinit var viewModel: GameViewModel
    lateinit var binding: FragmentGameBinding

   // override fun onSaveInstanceState(outState: Bundle) {
   //     super.onSaveInstanceState(outState)
    //    outState.putInt("score",ViewModel.score)
    //}

    //override fun onViewStateRestored(savedInstanceState: Bundle?) {
    //    super.onViewStateRestored(savedInstanceState)
    //    savedInstanceState?.let {
    //           ViewModel.score=savedInstanceState.getInt("score")
    //          showScore()
    //    }

    //}

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel=ViewModelProvider(this).get(GameViewModel::class.java)
        binding=FragmentGameBinding.inflate(inflater)
        binding.viewModel= viewModel
        binding.lifecycleOwner=this

//        showWord()

//        ViewModel.score.observe(viewLifecycleOwner, Observer {
//            showScore()
//        })


        viewModel.eventGameFinish.observe(viewLifecycleOwner, Observer {
            if (it==true){
                Toast.makeText(context, "go to next", Toast.LENGTH_SHORT).show()
                goToNextPage()
                viewModel.callGameFinished()
            }
        })


        binding.buttonCorrect.setOnClickListener {
            viewModel.score.value?.plus(1)
            viewModel.nextWord()

 //           showWord()

        }
        binding.buttonError.setOnClickListener {
            viewModel.score.value?.minus(1)
            viewModel.nextWord()
//            showWord()

        }



        return binding.root
    }

//    private fun showScore() {
//        binding.tvScore.text =ViewModel.score.value.toString()
//    }



 //   private fun showWord() {
 //       binding.tvWord.text= ViewModel.word
 //   }

    private fun goToNextPage() {
        findNavController().navigate(GameFragmentDirections
            .actionGameFragmentToScoreFragment(viewModel.score.value ?:0))
    }





}
