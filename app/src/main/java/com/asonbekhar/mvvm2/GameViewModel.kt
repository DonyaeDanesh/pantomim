package com.asonbekhar.mvvm2

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class GameViewModel : ViewModel(){
    private val _score= MutableLiveData<Int?>(0)
    val score : LiveData<Int?>
        get() = _score

    lateinit var wordList: MutableList<String>
    private val _word= MutableLiveData<String?>()
    lateinit var word :LiveData<String?>
    private val _eventGameFinish = MutableLiveData(false)
    val eventGameFinish: LiveData<Boolean>
        get() =_eventGameFinish



    init {
        _score.value=0
        creatWordList()
        nextWord()
    }


    fun nextWord(){
        if (wordList.isEmpty()){
           // goToNextPage()
            _eventGameFinish.value=true

        }else{
            _word.value=wordList.removeAt(0)

        }


    }


     fun creatWordList(){
        wordList= listOf<String>(
            "adc",
            "ac",
            "kjbascl",
            "kscmapmc",
            "pomca",
            ";lmcal;c",
            "kmcwoos",
            "lknckiw",
            "jdvnh",
            "klkjdinv",
            "p[,,as[c",
            "llsmvcs",
            "lmcpwsmcpws",
            "jbc",
            "kjbncud",
            "kncudbcu",
            "kncosnjpc",
            "yvy",
            "1111",
            "22222",
            "33333").toMutableList()
        wordList.shuffle()
    }
    fun callGameFinished (){
        _eventGameFinish.value=false

    }

}
